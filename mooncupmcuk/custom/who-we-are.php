<?php
/**
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Who we are main
 */

get_header(); ?>
<section class="single-col page-content primary" role="main">
		
	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>

	        <article class="container_full content_band">
	        	<div class="container_boxed--narrow">
	        	<?php the_field('1col_content_area');?>
	        	</div>
	        </article>

<div class="container_full">
<?php

// check if the repeater field has rows of data
if( have_rows('image_grid_general') ):?>
	

    <?php while ( have_rows('image_grid_general') ) : the_row();?>
	
	<div class="image-grid-block">
	        	<a href="<?php the_sub_field('tile_link_destination');?>" class="">
		        	<div class = "image-grid-item" style="background:linear-gradient(rgba(0, 0, 0, 0.2), rgba(0, 0, 0, 0.2)), url('<?php the_sub_field('grid_image'); ?>');">
			        	<div class="grid-content-container center text-reverse">
			        		<div class="grid-content">
				        	<?php
				        	the_sub_field('tile_text');
				        	?>
				        	</div>
			        	</div>
			        </div>
		    	</a>
	</div>
       

    <?php endwhile;?>
    
<?php 

else :

    // no rows found

endif;

?>
</div>	
</section>

<?php get_footer(); ?>
