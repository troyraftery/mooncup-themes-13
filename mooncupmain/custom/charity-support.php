<?php
/**
 *
 * @package WordPress
 * @subpackage Mooncup Main
 * @since Mooncup Main 1.0
 * Template Name: Chairty Support Main
 */

get_header(); ?>
<section class="single-col page-content primary" role="main">
		
	        <article class="container_full splash-content-block">
	        	<div class = "splash-image-narrow splash-image_generic image_fullwidth" style="background-image:url('<?php the_field('splash_image'); ?>');">
		        	<div class="splash-content-overlay splash-header text-reverse">
		        		<div class="container_full">
			        	<?php the_field('splash_content'); ?>
			        	</div>
		        	</div>
		        </div>
		    </article>


	        <article class="container_full content_band">
	        	<div class="container_boxed--narrow">
	        		<?php the_field('1col_content_area');?>
	        	</div>
	        </article>
			
			<div class="container_boxed">
			<?php

			// check if the repeater field has rows of data
			if( have_rows('charity_item') ):?>
			

			    <?php while ( have_rows('charity_item') ) : the_row();?>
				
				<div class="charity-item">
					<div class = "charity-item__image" style="background-image:url('<?php the_sub_field('charity_image');?>');">

					</div>
						        	
					<div class="charity-item__content">
						<h3 class="center">
							<?php
							the_sub_field('charity_title');
							?>
						</h3>
						<?php
							the_sub_field('charity_content');
						?>
					</div>

				</div>
						       
			    <?php endwhile;?>
			</div>
			<?php 

			else :

			    // no rows found

			endif;

			?>

			<aside class="page-outro container_boxed content_band--lined">
	        	<div class="container_boxed--narrow content_band--small">
	        		<?php the_field('footer_area');?>
	        	</div>
	        </aside>
	
</section>

<?php get_footer(); ?>
